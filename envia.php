<?php
 
// Primeiro incluímos os arquivos necessários:
require_once("classes/class.smtp.php");
require_once("classes/class.phpmailer.php");

// Aqui iniciamos as configurações do envio:
$mail = new PHPMailer(true);
 
// Habilitamos envio SMTP:
$mail->IsSMTP(); 
 
try {
     
     // Informamos o  endereço do servidor SMTP (o servidor de sua hospedagem)
     $mail->Host = 'br00.hostgator.com.br';

     // Habilitamos a autenticação SMTP
     $mail->SMTPAuth = true;

     // Habilitamos a encriptação TLS
     $mail->SMTPSecure = 'tls'; 

     //  Utilizamos a porta 587
     $mail->Port = 587;

     // O usuário deve ser seu endereço de email completo
     $mail->Username = 'contato@samuelhg.tk';

     // Senha da conta de email utilizada para envio
     $mail->Password = 'contato123';

     // Configuração de remetente (seu email)
     $mail->SetFrom('contato@samuelhg.tk', 'Samuel S. Website');

     // Endereço de resposta (seu email)
     $mail->AddReplyTo('contato@samuelhg.tk', 'Samuel S. Website');

     //Assunto do e-mail
     $mail->Subject = 'Teste PHPMailer SMTP';

     // Destinatário
     $mail->AddAddress('samuel.silva@endurance.com', 'Samuel S. HGBR');
 
     // As opções abaixo - Cópia, Cópia oculta e Anexo - são opcionais

     // Copia para
     //$mail->AddCC('destinarario@dominio.com.br', 'Destinatario');
     // Cópia Oculta para
     //$mail->AddBCC('destinatario_oculto@dominio.com.br', 'Destinatario2`'); 
     // Anexo
     //$mail->AddAttachment('images/phpmailer.gif');
 
     // Conteúdo (corpo) do email
     $mail->MsgHTML('Este email é um exemplo de envio via PHPMailer em SMTP.'); 
 
     // Pode-se utilizar um arquivo HTML como conteúdo do emaile
     //$mail->MsgHTML(file_get_contents('arquivo.html'));
     
     // Envio do email
     $mail->Send();

     // Mensagem de sucesso se o email for enviado corretamente
     echo "Mensagem enviada com sucesso</p>\n";
    }catch (phpmailerException $e) {

     // Mensagem de erro se houver problema no envio
      echo $e->errorMessage();
}

?>
